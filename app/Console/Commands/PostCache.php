<?php

namespace App\Console\Commands;

use App\Models\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PostCache
 * @package App\Console\Commands
 */
class PostCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache all Posts individually.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        Post::chunk(100, function (Collection $posts) {
            $this->line("Caching {$posts->count()} Posts.");

            $posts->each(function (Post $post) {
                $cacheKey = str_random();

                Cache::forever("post::{$post->id}", $post);
                Cache::forever("post::{$cacheKey}", $post);
            });
        });

        return 0;
    }
}
