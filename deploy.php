<?php

namespace Deployer;

require 'recipe/laravel.php';

/**
 * @mixin Deployer;
 */

use function Deployer\set;

/*
|--------------------------------------------------------------------------
| Variables
|--------------------------------------------------------------------------
|
*/

// Project name
set('application', 'Laravel');

// GIT Branch
set('branch', 'master');

// Number of Releases to keep.
set('keep_releases', 3);

// Project repository
set('repository', 'https://gitlab.com/ismaail/deployer-example.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

/*
|--------------------------------------------------------------------------
| Settings
|--------------------------------------------------------------------------
|
*/

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

/*
|--------------------------------------------------------------------------
| Hosts
|--------------------------------------------------------------------------
|
*/


host('192.168.1.80')
    ->user('deploy')
    ->set('deploy_path', '/var/www/laravel');

/*
|--------------------------------------------------------------------------
| Tasks
|--------------------------------------------------------------------------
|
*/

task('build', fn() => run('cd {{release_path}} && build'));

task('cachetool', fn() => run('sudo cachetool opcache:reset'));

/*
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
*/

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

// Clear PHP-FPM Opcache
after('deploy', 'cachetool');
