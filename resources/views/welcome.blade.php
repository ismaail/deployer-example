@php /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts */ @endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PHP Deployer</title>
        <link rel="stylesheet" href="{{ asset('assets/bootstrap.min.css') }}">
        <style>
            .posts-wrapper {
                max-height: 600px;
                overflow-y: scroll;
            }
        </style>
    </head>
    <body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQzIiBoZWlnaHQ9IjE5MyIgdmlld0JveD0iMCAwIDI0MyAxOTMiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBmaWxsPSIjMENGIiBkPSJNMjQyLjc4MS4zOUwuMjA3IDEwMS42NTNsODMuNjA2IDIxLjc5eiIvPjxwYXRoIGZpbGw9IiMwMEIzRTAiIGQ9Ik05Ny41NTUgMTg2LjM2M2wxNC4xMjktNTAuNTQzTDI0Mi43OC4zOSA4My44MTIgMTIzLjQ0MmwxMy43NDMgNjIuOTIyIi8+PHBhdGggZmlsbD0iIzAwODRBNiIgZD0iTTk3LjU1NSAxODYuMzYzbDMzLjc3My0zOS4xMTMtMTkuNjQ0LTExLjQzLTE0LjEzIDUwLjU0MyIvPjxwYXRoIGZpbGw9IiMwQ0YiIGQ9Ik0xMzEuMzI4IDE0Ny4yNWw3OC40ODQgNDUuNjY0TDI0Mi43ODIuMzkxIDExMS42ODMgMTM1LjgybDE5LjY0NCAxMS40MjkiLz48L2c+PC9zdmc+" alt="" height="20">
                    PHP Deployer
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link active" aria-current="page" href="#">Home</a></li>
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="#">About Us</a></li>
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="#">Contact</a></li>
                    </ul>
                    <form class="d-flex">
                        <button class="btn btn-outline-primary text-uppercase" type="submit">login</button>
                    </form>
                </div>
            </div>
        </nav>
    </header>
    <div class="container py-4">
        <h1 class="fw-bold">PHP Deployer</h1>
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('assets/frameworks.png') }}" alt="" class="img-fluid">
            </div>
            <div class="col-md-6">
                <div class="card posts-wrapper">
                    <div class="card-body bg-dark text-light">
                        <h5 class="card-title">Posts Titles</h5>
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach($posts as $post)
                            <li class="list-group-item">{{ $post->content }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
